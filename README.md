# image-optimizer

A lambda triggered via an S3 source bucket `putObject` calls (i.e. when new items are added to an s3 bucket) and then optimizes and creates thumbnails in a separate bucket.

## Technical info

This lambda uses the image processing library [sharp][sharp] and a transformers approach. Each transformer in [`src/transformers/*`](./src/transformers) will be applied, currently the following:

1. An optimize transformer that optimizes and translates extension to lowercase (e.g .JPG -> .jpeg)
1. A thumbnail transformer that translates to a 50 pixel width image

[sharp]: https://www.npmjs.com/package/sharp
