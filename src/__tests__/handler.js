import { optimize as handler } from '../handler';

test('it is a function', () => {
  expect(handler).toEqual(expect.any(Function));
});
