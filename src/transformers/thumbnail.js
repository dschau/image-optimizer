import sharp from 'sharp';

import { translateFilename } from '../util';

export const thumbnail = (response, key) => {
  const body = response.Body;

  const [name, ext] = key.split('.');

  return sharp(body)
    .resize(50)
    .jpeg({
      quality: 50
    })
    .toBuffer()
    .then(buffer => ({
      Body: buffer,
      ContentType: response.ContentType,
      Key: translateFilename(`${name}-thumb.${ext}`)
    }));
};
