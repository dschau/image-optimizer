import sharp from 'sharp';

import { translateFilename } from '../util';

export const optimize = (response, key) => {
  const body = response.Body;

  return sharp(body)
    .jpeg()
    .toBuffer()
    .then(buffer => ({
      Body: buffer,
      ContentType: response.ContentType,
      Key: translateFilename(key)
    }));
};
