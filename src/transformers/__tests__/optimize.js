jest.mock('sharp');
import { optimize } from '..';

test('it optimizes', async () => {
  const result = await optimize({
    Body: new Buffer('1234'),
    ContentType: 'text/jpeg'
  }, 'test.JPG');

  expect(result).toEqual({
    Body: expect.any(Buffer),
    ContentType: expect.any(String),
    Key: 'test.jpeg'
  });
});
