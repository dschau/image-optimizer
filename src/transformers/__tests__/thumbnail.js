jest.mock('sharp');
import { thumbnail } from '..';

test('it optimizes', async () => {
  const result = await thumbnail({
    Body: new Buffer('1234'),
    ContentType: 'text/jpeg'
  }, 'test.JPG');

  expect(result).toEqual({
    Body: expect.any(Buffer),
    ContentType: expect.any(String),
    Key: 'test-thumb.jpeg'
  });
});