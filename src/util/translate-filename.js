const EXT_MAP = {
  'JPG': 'jpeg'
};

export const translateFilename = filename => {
  let [name, ext] = filename.split('.');

  if (EXT_MAP[ext]) {
    ext = EXT_MAP[ext];
  } else {
    ext = ext.toLowerCase();
  }

  return [name, ext].join('.');
};
