import bunyan from 'bunyan';

export default bunyan.createLogger({ level: process.env.LOG_LEVEL, name: 'image-optimizer' });
