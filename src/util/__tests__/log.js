const getLog = (level = 'error') => {
  process.env.LOG_LEVEL = 'error';
  return require('../log').default;
};

test('it does not log if less than level', () => {
  const spy = jest.spyOn(console, 'warn');
  const log = getLog('error');

  log.warn('sup');

  expect(spy).not.toHaveBeenCalled();
});
