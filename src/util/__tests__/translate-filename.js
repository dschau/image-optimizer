import { translateFilename } from '..';

test('it replaces .JPG', () => {
  expect(translateFilename('test.JPG')).toBe('test.jpeg');
});

test('it lowercases extension', () => {
  [
    'test.JPEG',
    'test.PNG',
    'test.MP4'
  ]
    .forEach(filename => {
      expect(translateFilename(filename)).toEqual(filename.toLowerCase());
    });
});
