import sharp from 'sharp';
import AWS from 'aws-sdk';

import * as TRANSFORMERS from './transformers';
import { log } from './util';

const { DESTINATION_BUCKET: Bucket } = process.env;

export const optimize = async event => {
  if (!event.Records || event.Records.length === 0) {
    log.warn('No S3 record detected');
    return;
  }

  const s3 = new AWS.S3();

  try {
    return await Promise.all(event.Records.map(async record => {
      const { bucket, object } = record.s3;
      const { name: bucketName } = bucket;
      const { key: objectKey } = object;

      log.debug({
        bucketName,
        objectKey
      });

      const image = await s3.getObject({
        Bucket: bucketName,
        Key: objectKey
      }).promise();

      log.debug('Pulled image from s3');

      const allImages = await Promise.all(Object.keys(TRANSFORMERS).map(key => TRANSFORMERS[key](image, objectKey)));

      log.info('Optimized all images');

      return Promise.all(allImages.map(transformed => s3.putObject({
        Bucket,
        ...transformed
      }).promise()));
    }));
  } catch (e) {
    log.error(e);
    return e;
  }
};
