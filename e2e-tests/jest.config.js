module.exports = {
  testEnvironment: 'node',
  rootDir: '..',
  roots: [
    '<rootDir>/e2e-tests'
  ],
};
