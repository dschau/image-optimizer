import { config as loadDotEnv } from 'dotenv';
import AWS from 'aws-sdk';
import fs from 'fs';
import { optimize, thumbnail } from '../../src/transformers';

loadDotEnv();

const IMAGE_KEY = '04-10-2018-grand-canyon/IMG_0009.JPG';
const getImage = () => {
  const s3 = new AWS.S3();

  return s3.getObject({
    Bucket: 'photos-originals.dustinschau.com',
    Key: IMAGE_KEY
  }).promise();
};

test('it transforms to a thumbnail', async () => {
  const image = await getImage();

  const transformed = await thumbnail(image, IMAGE_KEY);

  expect(transformed).toEqual({
    Body: expect.any(Buffer),
    ContentType: 'image/jpeg',
    Key: expect.stringContaining('-thumb.jpeg')
  });
});

test('it optimizes an image', async () => {
  const image = await getImage();

  const transformed = await optimize(image, IMAGE_KEY);

  expect(transformed).toEqual({
    Body: expect.any(Buffer),
    ContentType: 'image/jpeg',
    Key: IMAGE_KEY.replace('.JPG', '.jpeg')
  });
});
