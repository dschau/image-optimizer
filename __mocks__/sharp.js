class Sharp {
  constructor(file) {
    this.file = file;
  }

  resize() {
    return this;
  }

  jpeg() {
    return this;
  }

  toBuffer() {
    return Promise.resolve(new Buffer(this.file));
  }
}

module.exports = jest.fn().mockImplementation((...args) => new Sharp(...args));
