resource "aws_lambda_function" "image-optimizer" {
  filename         = "../lambda.zip"
  function_name    = "image-optimizer"
  role             = "${aws_iam_role.iam_lambda_image_optimizer.arn}"
  handler          = "dist/handler.optimize"
  source_code_hash = "${base64sha256(file("../lambda.zip"))}"
  runtime          = "nodejs8.10"
  timeout          = "10"

  environment {
    variables = {
      DESTINATION_BUCKET = "${data.terraform_remote_state.infrastructure_misc.s3_bucket_photos_and_videos_name}"
      LOG_LEVEL = "debug"
      NODE_ENV = "production"
    }
  }
}

resource "aws_iam_role" "iam_lambda_image_optimizer" {
  name = "iam_lambda_image_optimizer"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "s3_policy" {
    name        = "s3_get_object_put_object_image_optimizer"
    description = "The policy to allow the lambda to get objects from s3 bucket"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetObject",
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": [
        "${data.terraform_remote_state.infrastructure_misc.s3_bucket_photos_and_videos_originals_arn}/*",
        "${data.terraform_remote_state.infrastructure_misc.s3_bucket_photos_and_videos_arn}/*"
      ]
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "s3_policy_attachment" {
  role = "${aws_iam_role.iam_lambda_image_optimizer.id}"
  policy_arn = "${aws_iam_policy.s3_policy.arn}"
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.image-optimizer.arn}"
  principal     = "s3.amazonaws.com"
  source_arn    = "${data.terraform_remote_state.infrastructure_misc.s3_bucket_photos_and_videos_originals_arn}"
}

resource "aws_s3_bucket_notification" "bucket_terraform_notification" {
  bucket = "${data.terraform_remote_state.infrastructure_misc.s3_bucket_photos_and_videos_originals_id}"
  lambda_function {
    lambda_function_arn = "${aws_lambda_function.image-optimizer.arn}"
    events = ["s3:ObjectCreated:*"]
  }
}
