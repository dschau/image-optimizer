data "terraform_remote_state" "infrastructure_misc" {
  backend = "s3"
  config {
    bucket    = "dschau-terraform-state"
    region    = "us-east-1"
    key       = "network/terraform-infrastructure-misc.tfstate"
  }
}
