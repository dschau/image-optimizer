provider "aws" {
  region  = "us-east-1"
  version = "1.14"
}

terraform {
  backend "s3" {
    bucket = "dschau-terraform-state"
    key    = "network/terraform-image-optimizer.tfstate"
    region = "us-east-1"
  }
}
